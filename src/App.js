import React, { Component } from 'react';
import './App.css';
import Modal from './Modal';

class App extends Component {

  state = {
    isModalVisible: false,
    isMouseOverBottomPart: false,
  }

  hide = () => {
    this.setState({ isModalVisible: false })
  }

  showWithDelay = () => {
    setTimeout(this.show, 50);
  }

  show = () => {
    const { isMouseOverBottomPart } = this.state;
    if (!isMouseOverBottomPart) {
      this.setState({ isModalVisible: true })
    }
  }

  handleEnterBottom = () => {
    this.setState({ isMouseOverBottomPart: true })
  }

  handleLeaveBottom = () => {
    this.setState({ isMouseOverBottomPart: false })
  }

  render() {
    const { isModalVisible } = this.state;
    return (
      <div className="App">
        <div
          className="top-part"
          onMouseLeave={this.showWithDelay}
        />
        <div
          className="bottom-part"
          onMouseEnter={this.handleEnterBottom}
          onMouseLeave={this.handleLeaveBottom}
        >
          <p className="App-intro">
            Move cursor to the browser tabs for modal dialog to be displayed
          </p>
          {isModalVisible ? <Modal onClose={this.hide}>Are you sure you want to leave?</Modal> : null }
        </div>
      </div>
    );
  }
}

export default App;
