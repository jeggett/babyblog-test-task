import React from 'react';

const Modal = ({children, onClose}) => (
  <div
    className="backdrop"
    ref={(backdrop) => {
        this.backdrop = backdrop;
    }}
    onClick={
      // Usually I'm using property initializers (like `hide` and `show` in App.js)
      // instead of arrow functions for callbacks to prevent memory leaks in IE
      (e) => {
        // Prevent firing on bubbled click events
        if (e.target === this.backdrop) {
          onClose();
        }
      }
    }
  >
    <div className="modal">
      <div className="modal-content">
        {children}
      </div>
      <div>
        <button onClick={onClose} className="button">
          Stay
        </button>
      </div>
    </div>
  </div>
);

export default Modal;